import 'dart:io';

void main(List<String> arguments) {
  String? Sentence = stdin.readLineSync()!;
  String Sentence1 = Sentence.toLowerCase();
  List<String> s = Sentence1.split(' ');
  var map = {};
  for (var word in s) {
    if (!map.containsKey(word)) {
      map[word] = 1;
    } else {
      map[word] += 1;
    }
  }

  print(map);
}
